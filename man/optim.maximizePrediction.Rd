% Generated by roxygen2 (4.0.2): do not edit by hand
\name{optim.maximizePrediction}
\alias{optim.maximizePrediction}
\title{Optimization criterion for maximal prediction}
\usage{
optim.maximizePrediction(matrixpls.res)
}
\arguments{
\item{matrixpls.res}{An object of class \code{matrixpls} from which the
criterion function is calculated}
}
\value{
Mean squared prediction error.
}
\description{
Optimization criterion for maximal prediction
}
\details{
Calculates the predicted variances of reflective indicators. The
prediction criterion is negative of the sum of predicted variances.
}
\seealso{
Other Weight optimization criteria: \code{\link{optim.GCCA}};
  \code{\link{optim.GSCA}};
  \code{\link{optim.maximizeInnerR2}}
}

